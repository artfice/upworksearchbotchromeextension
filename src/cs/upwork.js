console.log('start upwork script');

var ignoreCountry = ['India', 'Pakistan', 'United Arab Emirates', 'Israel', 
'South Africa', 'Aruba', 'Singapore', 'Nigeria', 'Brazil', 'Ukraine', 'Jordan',
'Saudi Arabia', 'Bangladesh', 'Russia', 'Hong Kong', 'Philippines', 'Egypt',
'Macedonia', 'Isle of Man', 'Lebanon', 'Bulgaria', 'Malaysia', 'Vanuatu', 'Peru',
'Vietnam'];

function strip(html) {
    var tmp = document.createElement('div');
    tmp.innerHTML = html;
    return (tmp.textContent || tmp.innerText).trim();
}

var list = Array.prototype.slice.call(document.querySelectorAll('section.job-tile'));
// var list = Array.prototype.slice.call(document.querySelectorAll('article.job-tile'));
list = list.map(function(row){
    var html = row.innerHTML;
    var numFreelancer = 1;

    var titleStartPos = html.indexOf('<a data-ng-bind-html');
    var titleEndPos = html.indexOf('</a>');
    var titleRaw = html.substring(titleStartPos, titleEndPos);
    var titleParsed = titleRaw.substring(titleRaw.indexOf('>') + 1, titleRaw.length);

    var hrefStartPos = titleRaw.indexOf('itemprop="url" href="');
    var hrefEndPos = titleRaw.indexOf('">');
    var hrefRaw = titleRaw.substring(hrefStartPos + 21, hrefEndPos);

    var descriptionStart = html.indexOf('<div data-eo-truncation-html-unsafe');
    var descriptionEnd = html.indexOf('less</a>');
    var descriptionRaw = html.substring(descriptionStart, descriptionEnd);

    var ds = descriptionRaw.indexOf('"truncatedHtml">');
    var de = descriptionRaw.lastIndexOf('</span>');
    descriptionRaw = descriptionRaw.substring(ds + 16, de);

    var isFixed = html.indexOf('<strong data-ng-if="::jsuJobTypeController.isFixed()" class="js-type">Fixed-Price</strong>');
    var hourType = 'hourly';
    var budget = 0;
    if (isFixed > -1 ) {
        hourType =  'fixed';
        var budgetStart = html.indexOf('itemprop="baseSalary">');
        var budgetRaw = '<span ' + html.substring(budgetStart, budgetStart + 40);
        budgetRaw = strip(budgetRaw);
        budgetRaw = budgetRaw.replace('$', '').replace(',', '');
        budget = parseFloat(budgetRaw.replace( /^\D+/g, ''));        
    }

    // var ps = html.indexOf('<time data-eo-relative="');
    // var pe = html.indexOf('" itemprop="datePosted"');
    // timeRaw = html.substring(ps + 24, pe);
    var ps = html.indexOf('<time data-eo-relative="');
    var pe = html.indexOf('" data-short');
    timeRaw = html.substring(ps + 24, pe);

    if (html.indexOf('<span class="text-muted">Number of freelancers needed:</span>') > 0) {
        numFreelancer = 2;
    }
    
    var countryStart = html.indexOf('<span class="glyphicon glyphicon-md air-icon-location m-0"></span>');
    var countryEnd = countryStart + 66 + 140;
    var countryRaw = html.substring(countryStart, countryEnd);

    var country = strip(countryRaw);

    return {title: titleParsed, type: hourType, budget: budget, time: timeRaw, url: hrefRaw, description: descriptionRaw, numFreelancer: numFreelancer, country: country};
}).filter(function(job){
    var country = ignoreCountry.indexOf(job.country) < 0;
    var freelancers = (job.numFreelancer == 1);
    var condition = freelancers && country;
    return condition;
});
console.log('end upwork script', list);

var ajax = {};
ajax.x = function () {
    if (typeof XMLHttpRequest !== 'undefined') {
        return new XMLHttpRequest();
    }
    var versions = [
        "MSXML2.XmlHttp.6.0",
        "MSXML2.XmlHttp.5.0",
        "MSXML2.XmlHttp.4.0",
        "MSXML2.XmlHttp.3.0",
        "MSXML2.XmlHttp.2.0",
        "Microsoft.XmlHttp"
    ];

    var xhr;
    for (var i = 0; i < versions.length; i++) {
        try {
            xhr = new ActiveXObject(versions[i]);
            break;
        } catch (e) {
        }
    }
    return xhr;
};

ajax.send = function (url, callback, method, data, async) {
    if (async === undefined) {
        async = true;
    }
    var x = ajax.x();
    x.open(method, url, async);
    x.onreadystatechange = function () {
        if (x.readyState == 4) {
            callback(x.responseText)
        }
    };
    if (method == 'POST') {
        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    }
    x.send(data)
};

ajax.get = function (url, data, callback, async) {
    var query = [];
    for (var key in data) {
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }
    ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async)
};

ajax.post = function (url, data, callback, async) {
    var query = [];
    for (var key in data) {
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }
    ajax.send(url, callback, 'POST', query.join('&'), async)
};

ajax.post('https://upwork.manaknightdigital.com/api/upwork/posting', {
job_posting: JSON.stringify(list)
}, function(a) { console.log(a)});
            
setTimeout(function(){ 
    location.reload(); 
}, 60000);